#!/bin/bash
set -u

sizes=(256 512 1024 2048);
formats=("webp" "jpeg");

# EXIF strip the original image
echo "stripping exif data from input (but the ICC profile)"
#set -o xtrace
exiftool -all= -TagsFromFile @ -ColorSpaceTags $1 -o "$1".temp
#set +o xtrace

# Trim input filename
name=$(echo "$1" | cut -f 1 -d '.')

# Identify 
size_raw=`identify -format "%wx%h" "$1"`;
identified=($(echo $size_raw | grep -o "[0-9]*"));
width=${identified[0]};
height=${identified[1]};

maxdim=0;
if (( width > height ))
then
	maxdim=$width;
else
	maxdim=$height;
fi
sizes+=($maxdim);

# Find which target sizes (if any) should be generated
for item in ${sizes[*]}
do
	echo "checking whether to target $item... "
	if (( item <= maxdim ))
	then
		echo "YES"
		for format in ${formats[*]}
		do
			echo "converting to $format with maximum dimension of $item: \n"
			#set -o xtrace
			convert "$1".temp -resize "${item}x${item}" "${name}_${item}.${format}"
			#set +o xtrace
		done
	else
		echo "NO"
	fi
done

# Finished
rm "$1".temp
		
